#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <utime.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "ar80.h"
#include "bitstream.h"

char *outfilename;
static char *tmpfilename;

static void list_member(struct arch_member *member);
static char *get_module_name(struct object_item *head);
static int find_member(char *membername, char **argv);

BITFILE *open_tmp(void)
{
	int fd = -1;
	BITFILE *retval;
	int i;

	for (i=0; i<3 && fd==-1; i++) {
		tmpfilename = tmpnam(NULL);
		fd = open(tmpfilename, O_RDWR|O_CREAT|O_EXCL, S_IRUSR|S_IWUSR);
	}
	if (fd == -1) return NULL;

	retval = bit_open(tmpfilename, "w");
	close(fd);
	if (retval == NULL) unlink(tmpfilename);
	return retval;
}

void copy_tmp(char *libfilename)
{
	char cmd[1024];

	sprintf(cmd, "cp %s %s",tmpfilename, libfilename);
	system(cmd);
}

void list_archive(struct arch_member *member, char **argv)
{
	while (member) {
		if (*argv ? (find_member(member->basename, argv)!=-1) : 1)
			list_member(member);
		member = member->next;
	}
}

struct object_item *copy_lib(BITFILE *libfile, BITFILE *outfile,
		struct object_item *head, char *before, char *after,
		char mode, char **exceptions)
{
	IFDEBUG(1, printf("copy_lib(libfile=%p, outfile=%p, head=%p, before=%s, after=%s, mode=%c, exceptions=%p)\n", libfile, outfile, head, before, after, mode ? mode : '0', exceptions));
	if (!head) head = read_module(libfile, NULL);
	while (head) {
		struct object_item *item;
		char *membername = get_module_name(head);
		int break_loop;

		if (before && !strcmp(before,membername)) break;
		if (exceptions && find_member(membername, exceptions)!=-1) {
			IFDEBUG(1, printf("dropping %s\n", membername));
			if (verbose && mode)
				printf("%c - %s\n", mode, membername);
		} else {
			IFDEBUG(1, printf("copying %s\n", membername));
			for (item=head; item!=NULL; item=item->next)
				write_module(item, outfile);
		}
		break_loop = after && !strcmp(after,get_module_name(head));
		free_module(head); head = NULL;
		if (break_loop) break;
		head = read_module(libfile, NULL);
	}
	return head;
}

int add_member(BITFILE *outfile, char *objfilename, char mode)
{
	int i;
	struct stat s;
	struct object_item attr = {
		type: T_RELOCATABLE|T_SPECIAL,
		v: { special: { control: C_EXTENSION } }
	};
	struct object_item *head, *item;
	BITFILE *member;

	member = bit_open(objfilename, "r");
	if (member == NULL) {
		fatalerror = 1;
		fprintf(stderr, "%s: ", progname);
		perror(objfilename);
		return 0;
	}
	IFDEBUG(1, printf("adding %s\n", objfilename));
	if (verbose) printf("%c - %s\n", mode, objfilename);

	// Set up default attributes
	fstat(fileno(member->f), &s);
	attr.v.special.B_len = 3;
	attr.v.special.B_name[0] = 'p';
	attr.v.special.B_name[1] = s.st_mode & 0xff;
	attr.v.special.B_name[2] = (s.st_mode >> 8) & 0x01;
	write_module(&attr, member);
	attr.v.special.B_len = 1 + sizeof(time_t);
	attr.v.special.B_name[0] = 't';
	for (i=0; i<sizeof(time_t); i++)
		attr.v.special.B_name[i+1] = (s.st_mtime >> i*8) & 0xff;
	write_module(&attr, member);
	attr.v.special.B_len = 1 + sizeof(uid_t);
	attr.v.special.B_name[0] = 'o';
	for (i=0; i<sizeof(uid_t); i++)
		attr.v.special.B_name[i+1] = (s.st_uid >> i*8) & 0xff;
	write_module(&attr, member);
	attr.v.special.B_len = 1 + sizeof(gid_t);
	attr.v.special.B_name[0] = 'g';
	for (i=0; i<sizeof(gid_t); i++)
		attr.v.special.B_name[i+1] = (s.st_gid >> i*8) & 0xff;
	write_module(&attr, member);

	for (item=head=read_module(member, NULL); item; item=item->next)
		write_module(item, outfile);
	free_module(head);

	bit_close(member);
	return 1;
}

void extract_members(BITFILE *libfile, char **argv, struct object_item **modv)
{
	struct object_item *head;

	while ((head=read_module(libfile, NULL))!=NULL) {
		int i;
		char *membername = get_module_name(head);
		if ((i=find_member(membername, argv))!=-1) {
			IFDEBUG(1, printf("extracting %s\n", membername));
			modv[i] = head;
		}
	}
}

void save_all_modules(BITFILE *libfile, int orig)
{
	struct object_item *head;

	while ((head=read_module(libfile, NULL))!=NULL) {
		save_module(head, orig);
		free_module(head);
	}
}

void save_module(struct object_item *head, int orig)
{
	struct object_item *item;
	char *membername = get_module_name(head);
	BITFILE *outfile;
	uid_t u;
	gid_t g;
	time_t t;
	int p;
	struct utimbuf utbuf;

	IFDEBUG(1, printf("saving %s\n", membername));
	if (verbose) printf("x - %s\n", membername);
	outfile = bit_open(membername, "w");

	for (item=head; item; item=item->next) {
		if (item->type==(T_RELOCATABLE|T_SPECIAL) &&
			item->v.special.control==C_EXTENSION) {
			switch (item->v.special.B_name[0]) {
			case 'p':
				p = mode_item(item);
				continue;
			case 't':
				t = time_item(item);
				continue;
			case 'o':
				u = owner_item(item);
				continue;
			case 'g':
				g = group_item(item);
				continue;
			default:
				break;
			}
		}
		write_item(item, outfile);
	} // for
	write_eof(outfile);
	bit_close(outfile);
	utbuf.modtime = utbuf.actime = t;
	if (orig) utime(membername, &utbuf);
	chmod(membername, p);
	chown(membername, u, g);
}

void write_module(struct object_item *item, BITFILE *outfile)
{
	static struct object_item	*perm_item=NULL,
					*time_item=NULL,
					*owner_item=NULL,
					*group_item=NULL;

	switch (item->type) {
	case T_ABSOLUTE:
	case T_RELOCATABLE|T_CODE:
	case T_RELOCATABLE|T_DATA:
	case T_RELOCATABLE|T_COMMON:
		break;
	case T_RELOCATABLE|T_SPECIAL:
		switch (item->v.special.control) {
		case C_END_FILE:
			return;
		case C_EXTENSION:

#define STORE_ATTRIB(t) if (t##_item) free(t##_item); \
			t##_item = malloc(sizeof(*item)); \
			memcpy(t##_item, item, sizeof(*item))

			switch (item->v.special.B_name[0]) {
			case 'p':
				STORE_ATTRIB(perm);
				return;
			case 't':
				STORE_ATTRIB(time);
				return;
			case 'o':
				STORE_ATTRIB(owner);
				return;
			case 'g':
				STORE_ATTRIB(group);
				return;
			default:
				break;
			}
			break;
		case C_END_PROGRAM:

#define WRITE_ATTRIB(t) if (t##_item) { \
				write_item(t##_item, outfile); \
				t##_item = NULL; \
			}

				WRITE_ATTRIB(perm);
				WRITE_ATTRIB(time);
				WRITE_ATTRIB(owner);
				WRITE_ATTRIB(group);
				break;
		default:
			break;
		}
		break;
	}

	write_item(item, outfile);
}

void write_eof(BITFILE *f)
{
	static struct object_item end_of_file = {
		type: T_RELOCATABLE|T_SPECIAL,
		v: { special: { control: C_END_FILE } }
	};
	write_item(&end_of_file, f);
}

static void list_member(struct arch_member *member)
{

	if (verbose) {
		int i;
		char tbuf[18];
		char pbuf[] = "rwxrwxrwx";

		for (i=0; i<9; i++) if (!(member->st.st_mode&(1<<i)))
			pbuf[8-i] = '-';
		strftime(tbuf, sizeof(tbuf), "%h %e %R %Y",
			localtime(&member->st.st_mtime));
		printf("%s %d/%d %6lu %s ", pbuf,
			member->st.st_uid, member->st.st_gid,
			(unsigned long)align_len(member->bitsize)/8, tbuf);
	}
	printf("%s\n", member->basename);
}

static char *get_module_name(struct object_item *head)
{
	static char modname[13];
	char *d = modname;
	char *s;

	if (head->type != (T_RELOCATABLE|T_SPECIAL)) return NULL;
	if (head->v.special.control != C_PROGNAME) return NULL;

	for (d=modname, s=head->v.special.B_name; *s; s++,d++)
		*d = tolower(*s);
	for (s=".rel"; (*d=*s)!='\0'; s++,d++) /* EMPTY */;
	return modname;
}

static int find_member(char *membername, char **argv)
{
	int i;

	for (i=0; argv[i]; i++) if (!strcmp(membername, argv[i])) break;
	return argv[i] ? i : -1;
}

struct arch_member *read_member(BITFILE *bf, char *filename)
{
	struct arch_member *node;
	struct object_item *prev, *item;

	node = calloc_or_die(1, sizeof(struct arch_member));
	if (!bf) {
		char *p;
		bf = bit_open(filename, "r");
		if (!bf) {
			fprintf(stderr, "%s: ", progname);
			perror(filename);
			fatalerror = 1;
			return NULL;
		}
		node->path=strdup(filename);
		if (!node->path) {
			fprintf(stderr, "%s: not enough memory\n", progname);
			fatalerror = 1;
			return NULL;
		}
		p = strrchr(node->path, '/');
		if (p==NULL) {
			node->basename = node->path;
			node->path = strdup(".");
		}
		else *p = '\0';
	}
	node->bitpos = bit_tell(bf);
	fstat(fileno(bf->f), &(node->st));
	node->head = read_module(bf, &node->bitsize);
	if (node->head == NULL) {
		free(node);
		return NULL;
	}

	if (!node->basename)
		node->basename = strdup(get_module_name(node->head));

	prev=(struct object_item*)&node->head;
	item=node->head;
	while (item) {
		if (item->type==(T_RELOCATABLE|T_SPECIAL) &&
			item->v.special.control==C_EXTENSION) {
			switch (item->v.special.B_name[0]) {
			case 'p':
				node->st.st_mode = mode_item(item);
				break;
			case 't':
				node->st.st_mtime = time_item(item);
				break;
			case 'o':
				node->st.st_uid = owner_item(item);
				break;
			case 'g':
				node->st.st_gid = group_item(item);
				break;
			default:
				goto not_delete;
			}
			node->bitsize -= 10 + item->v.special.B_len;
			// delete item
			prev->next = item->next;
			free(item);
			item = prev->next;
			continue;
		}
not_delete:
		item=item->next;
		prev=prev->next;
	}
	return node;
}

int write_member(struct arch_member *member, BITFILE *outfile)
{
	int i;
	struct stat s;
	struct object_item attr = {
		type: T_RELOCATABLE|T_SPECIAL,
		v: { special: { control: C_EXTENSION } }
	};
	struct object_item *head, *item;

	for (item=member->head; item && item->next; item=item->next)
		write_item(item, outfile);

	attr.v.special.B_len = 3;
	attr.v.special.B_name[0] = 'p';
	attr.v.special.B_name[1] = member->st.st_mode & 0xff;
	attr.v.special.B_name[2] = (member->st.st_mode >> 8) & 0x01;
	write_item(&attr, outfile);

	attr.v.special.B_len = 1 + sizeof(time_t);
	attr.v.special.B_name[0] = 't';
	for (i=0; i<sizeof(time_t); i++)
		attr.v.special.B_name[i+1] = (member->st.st_mtime >> i*8) & 0xff;
	write_item(&attr, outfile);

	attr.v.special.B_len = 1 + sizeof(uid_t);
	attr.v.special.B_name[0] = 'o';
	for (i=0; i<sizeof(uid_t); i++)
		attr.v.special.B_name[i+1] = (member->st.st_uid >> i*8) & 0xff;
	write_item(&attr, outfile);

	attr.v.special.B_len = 1 + sizeof(gid_t);
	attr.v.special.B_name[0] = 'g';
	for (i=0; i<sizeof(gid_t); i++)
		attr.v.special.B_name[i+1] = (member->st.st_gid >> i*8) & 0xff;
	write_item(&attr, outfile);

	if (item) write_item(item, outfile);

	return 1;
}
