#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <ctype.h>
#include "ar80.h"

enum cmd_code arcmd = CMD_NONE;
int debug, verbose, create;
static char *libfilename;
BITFILE *libfile, *outfile;
int fatalerror = 0;;
char *progname = "ar80";

void usage(void);

int main(int argc,char **argv)
{
	int c, i, abort = 0, modify_archive = 0, original = 0;
	char *before = NULL, *after = NULL;
	struct object_item *cont, **modv;
	char **f;
	struct arch_member *archhead, *archtail, *objhead, **p, *q;

#define	REGULAR_OPTSTRING	"hdmqrtxa:b:cfi:N:ouvV"
#ifdef	DEBUG
#define	OPTSTRING		REGULAR_OPTSTRING "D"
#else
#define	OPTSTRING		REGULAR_OPTSTRING
#endif

	while ((c = getopt (argc, argv, OPTSTRING)) != -1) switch (c) {
	case 't':	/* Display content of archive */
		arcmd = CMD_TABLE;
		break;
	case 'q':	/* Quick append */
		arcmd = CMD_APPEND;
		modify_archive = 1;
		break;
	case 'r':	/* Replace archive members */
		arcmd = CMD_REPLACE;
		modify_archive = 1;
		break;
	case 'm':	/* Move members within archive */
		arcmd = CMD_MOVE;
		modify_archive = 1;
		break;
	case 'd':	/* Delete archive member*/
		arcmd = CMD_DELETE;
		modify_archive = 1;
		break;
	case 'x':	/* Extract archive member */
		arcmd = CMD_EXTRACT;
		break;
	case 'V':	/* Version */
		die(E_SUCCESS, "%s v%s\n", progname, VERSION);
		/* NOT REACHED */
		break;
#ifdef	DEBUG
	case 'D':	/* Debug level */
		debug++;
		break;
#endif
	case 'v':	/* Verbose operation */
		verbose = 1;
		break;
	case 'c':	/* Create archive */
		create = 1;
		break;
	case 'o':	/* Preserve original date */
		original = 1;
		break;
	case 'i':	/* Insert ... */
	case 'b':	/* ... before member */
		after = NULL;
		before = optarg;
		break;
	case 'a':	/* Insert after member */
		before = NULL;
		after = optarg;
		break;
	default:
		abort = 1;
		/* FALL THROUGH */
	case 'h':	/* Help */
		usage();
		die(abort ? E_USAGE : E_SUCCESS, "");
		/* NOT REACHED */
		break;
	} /* switch(c) */

	if (optind>=argc) die(E_USAGE,"%s: No archive given\n", progname);
	libfilename = argv[optind++];

	argv += optind;
	argc =- optind;

	libfile = bit_open(libfilename, "r");
	if (libfile == NULL) {
		if (!(modify_archive && errno == ENOENT))
			die(E_USAGE, "%s: Cannot open archive '%s'\n",
				progname, libfilename);
		if (!create) fprintf(stderr,"%s: creating %s\n",
				progname, libfilename);
		archtail = archhead = NULL;
	}
	else {
		for (p=&archhead, archtail=NULL;
				(*p=read_member(libfile, NULL));
				archtail=*p, p=&((*p)->next));
	}
	if (modify_archive) {
		outfile = open_tmp();
		if (outfile == NULL)
			die(E_SYSTEM, "%s: Cannot open tmp file\n", progname);
	}

	switch (arcmd) {
	case CMD_TABLE:
	case CMD_EXTRACT:
		break;
	case CMD_MOVE:
	case CMD_DELETE:
		break;
	case CMD_REPLACE:
	case CMD_APPEND:
		for (p=&objhead, f=argv; *f; p=&((*p)->next), f++)
				(*p=read_member(NULL, *f));
		break;
	}

	switch (arcmd) {
	case CMD_TABLE:
		list_archive(archhead, argv);
		break;
	case CMD_APPEND:
		if (verbose)
			for (q=objhead; q; q=q->next)
				printf("a - %s\n", q->basename);
		p = archtail ? (&archtail->next) : (&archhead);
		*p = objhead;
		for (q=archhead; q; q=q->next) write_member(q, outfile);
		break;
	case CMD_DELETE:
		copy_lib(libfile, outfile, NULL, NULL, NULL, 'd', argv);
		break;
	case CMD_MOVE:
		if (!argv[0]) {
			fprintf(stderr, "nem adtal meg tagokat\n");
			fatalerror = 1;
			break;
		}
		modv = calloc_or_die(argc-optind, sizeof(struct object_item*));
		extract_members(libfile, argv, modv);
		bit_seek(libfile, 0, SEEK_SET);
		cont = copy_lib(libfile, outfile, NULL,
				before, after, 'm', argv);
		for (i=0; i<argc-optind && !fatalerror; i++) {
			struct object_item *item;
			for (item=modv[i]; item && !fatalerror; item=item->next)
				write_module(item, outfile);
		}
		if (fatalerror) break;
		copy_lib(libfile, outfile, cont, NULL, NULL, 'm', argv);
		break;
	case CMD_REPLACE:
		cont = copy_lib(libfile, outfile, NULL,
				before, after, 'r', argv);
		for (f=argv; *f; f++)
                        if (!add_member(outfile, *f, 'R')) break;
		if (fatalerror) break;
		copy_lib(libfile, outfile, cont, NULL, NULL, 'r', argv);
		break;
	case CMD_EXTRACT:
		if (argv[0]) {
			modv = calloc_or_die(argc, sizeof(struct object_item*));
			extract_members(libfile, argv, modv);
			for (i=0; i<argc-optind; i++) {
				if (!modv[i]) {
					fprintf(stderr,"%s: no member %s\n",
						progname, argv[i]);
					fatalerror = 1;
					break;
				}
				save_module(modv[i], original);
			}
		}
		else save_all_modules(libfile, original);
		break;
	case CMD_NONE:
		die(E_USAGE,"%s: no operation specified\n", progname);
		/* NOT REACHED */
		break;
	}
	if (modify_archive) {
		write_eof(outfile);
		bit_close(outfile);
		if (!fatalerror) copy_tmp(libfilename);
	}

	die(fatalerror ? E_INPUT : E_SUCCESS, "");
}

void *calloc_or_die(size_t nmemb, size_t size)
{
	void *retval = calloc(nmemb, size);
	if (retval==NULL) die(E_RESOURCE,"%s: not enough memory\n", progname);
	return retval;
}

void usage(void)
{
	fprintf(stderr,
"Usage:\n"
"ar80 cmd command [options] archive [files ...]\n"
"Commands:\n"
" -t         print Table.\n"
" -q         Quick append.\n"
" -r         Replace member.\n"
" -d         Delete from archive.\n"
" -x         eXtract archive members.\n"
" -V         print Version number and exit.\n"
" -h         print this Help message and exit\n"
"Options:\n"
" -v         Verbose operation.\n"
#ifdef DEBUG
" -D         Debug. (More -D results more debug info printed.)\n"
#endif
" -a member  add new modules After 'member'.\n"
" -b member  add new modules Before 'member'.\n"
" -c         Create archive.\n"
" -o         preserve Original dates during extraction.\n"
	);
}

void die(int status, const char *format, ...)
{
	va_list arg;

	fflush(stdout);
	va_start(arg, format);
	vfprintf(stderr, format, arg);
	va_end(arg);

//	if (status && ofilename) unlink(ofilename);
//	if (status && symfilename) unlink(symfilename);
//printf("exit(%d)\n", status);
	exit(status);
}

