BASEDIR		= /usr/local
BINDIR		= $(BASEDIR)/bin
MANDIR		= $(BASEDIR)/man/man1
INSTALL		= install

VERSION		= 0.50i2
CFLAGS		= -g -Wall -Wstrict-prototypes -DVERSION=\"$(VERSION)\"
CFLAGS		:= $(CFLAGS) -DDEBUG

LD80OBJS	= ld80.o bitstream.o obj80.o readobj.o section.o symbol.o fixup.o do_out.o
AR80OBJS	= ar80.o bitstream.o obj80.o copyobj.o
BINARIES	= ld80    ar80
MANPAGES	= ld80.1  ar80.1
PSFILES		= ld80.ps ar80.ps

.SUFFIXES: .pod .1 .html .ps

all:		$(BINARIES) $(MANPAGES)

ld80:		$(LD80OBJS)
	gcc $(CFLAGS) -o ld80 $(LD80OBJS)

ar80:		$(AR80OBJS)
	gcc $(CFLAGS) -o ar80 $(AR80OBJS)

clean:
	rm -f *.o pod2html-*cache core

distclean:	clean
	rm -f $(MANPAGES) $(PSFILES) $(BINARIES)

install:	all
	$(INSTALL) -s -c -m 755 $(BINARIES) $(BINDIR)
	$(INSTALL) -s -c -m 644 $(MANPAGES) $(MANDIR)
tar:
	tar -cvzf /tmp/ld80-$(VERSION).tgz *.c *.h ld80* ar80* Makefile

man:		$(MANPAGES)

ps:		$(PSFILES)

.pod.1:
	pod2man --center=' ' --release='$* $(VERSION)' \
		--section=1 $*.pod > $*.1

.pod.html:
	pod2html --noindex $*.pod | sed 's/<HR>//g' > $*.html

.1.ps:
	troff -man $*.1 | grops > $*.ps

depend dep:
#	makedepend -f- $(SOURCES) > .depend
	$(CC) $(ALLFLAGS) -MM *.c > .depend

-include .depend
