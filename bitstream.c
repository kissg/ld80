#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "bitstream.h"
#include "ld80.h"
extern int debug;

BITFILE *bit_open(const char *path, const char *mode)
{
	BITFILE *bf;

	bf = malloc(sizeof(*bf));
	if (bf == NULL) return NULL;
	bf->flags = 0;

	bf->f = fopen(path, mode);
	if (bf->f == NULL) {
		free(bf);
		return NULL;
	}

	if (!(mode[0]=='r' && mode[1]=='\0')) bf->flags |= BIT_WRITE;
	bf->bytebuf = 0;
	bf->bitpos = 0;
	IFDEBUG(4, printf("bit_open(%s,\"%s\")\n", path, mode));
	return bf;
}

int put_bit(unsigned bit, BITFILE *bf)
{
	int retval;

	IFDEBUG(8, printf("%d: -> %d\n", fileno(bf->f), (bit != 0)));
	bf->bytebuf <<= 1;
	bf->bytebuf |= (bit != 0);
	bf->bitpos++;
	if (bf->bitpos%8) return 1;

	IFDEBUG(5, printf("%d: -> 0x%.2x\n", fileno(bf->f), bf->bytebuf));
	retval = bf->bytebuf == fputc(bf->bytebuf, bf->f);
	bf->bytebuf = 0;
	return retval;
}

int get_bit(BITFILE *bf)
{
	int retval;

	if (bf->bitpos%8 == 0) {
		int c = fgetc(bf->f);
		if (c==EOF) return EOF;
		bf->bytebuf = c;
		IFDEBUG(5, printf("%d: <- 0x%.2x\n",
				fileno(bf->f), bf->bytebuf));
	}
	retval = (bf->bytebuf & 0x80)!=0;
	IFDEBUG(8, printf("%d: <- %d\n", fileno(bf->f), retval));
	bf->bytebuf <<= 1;
	bf->bitpos++;
	return retval;
}

int bit_write(unsigned long bits, int len, BITFILE *bf)
{
	int cnt=0;

	IFDEBUG(6, printf("bit_write(0x%lx, %d)\n", bits, len));
	while (len--) {
		if (put_bit(bits & (1 << len), bf) < 1) break;
		cnt++;
	}
	IFDEBUG(6, printf("bytebuf=0x%x count=%ld\n", bf->bytebuf, bf->bitpos%8));
	return cnt;
}

int bit_read(unsigned char *bits, int len, BITFILE *bf)
{
	int cnt=0, c;
	int len2 = len;

	while (len--) {
		if (!(cnt%8)) *bits = 0;
		c = get_bit(bf);
		if (c==EOF) break;
		*bits = (*bits << 1) | c;
		cnt++;
		if (!(cnt%8) && debug>5) {
			printf(" bit_read(%d)=0x%x\n", 8, *bits);
			len2-=8;
		}
		if (!(cnt%8)) bits++;
	}
	if (len2 && debug>5) printf("bit_read(%d)=0x%x\n", len2, *bits);
	return cnt;
}

int bit_align(BITFILE *bf)
{
	unsigned char dummy;

	if (!(bf->bitpos%8)) return 0;
	if (bf->flags & BIT_WRITE)
		return bit_write(0, 8 - bf->bitpos%8, bf);
	else
		return bit_read(&dummy, 8 - bf->bitpos%8, bf);
}

bitpos_t bit_tell(BITFILE *bf)
{
	return bf->bitpos;
}

int bit_seek(BITFILE *bf, bitpos_t offset, int whence)
{
	unsigned char dummy;

	if ((bf->flags&BIT_WRITE && offset%8) || whence != SEEK_SET) {
		errno = EINVAL;
		return -1;
	}
	if (fseek(bf->f, offset/8, SEEK_SET)) return -1;
	bf->bitpos = offset & ~(8-1);
	bit_read(&dummy, offset%8, bf);
	return 0;
}

int bit_close(BITFILE *bf)
{
	int retval;

	bit_align(bf);
	retval = fclose(bf->f);
	free(bf);
	return retval;
}

int bit_eof(BITFILE *bf, int cmd)
{
	switch (cmd) {
	case BIT_EOF_SET:
		bf->flags |= BIT_EOF;
		break;
	case BIT_EOF_GET:
	default:
		break;
	}
	return bf->flags & BIT_EOF;
}
