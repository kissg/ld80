=head1 NAME

ld80 - L80 replacement linker

=head1 SYNOPSYS

B<ld80> B<-o> I<outfile> [B<-O> I<oformat>] [B<-W> I<warns>]
[B<-s> I<symfile>] [B<-S> I<symsize>] [B<-cmV>] [B<-U> name]
[B<-l>] [B<-P> I<address>] [B<-D> I<address>]
[B<-C> I<name>B<,>I<address>] I<objectfile> ...

=head1 DESCRIPTION

B<ld80> is a replacement of the good old M$ L80 linker for CP/M
and other 8 bit operation systems on Z80/i8080 processor.
B<ld80> incorporates almost all capabilities of L80 and has extra
features. It is intended to be part of your Z80 cross development toolbox.

B<ld80> accepts object (.REL) files produced with M80 or compatible
assembler as well as library (.LIB) files made by LIB80.

=head1 OPTIONS

Addresses below are within the 0000-FFFF range and are expressed
in hexadecimal notation. Addresses prefixed by percent (%) character
as %Z<>NNNN are not absolute but the linker is instructed to place
following segments on NNNN-boundary. E.g. -Z<>D%10 causes the
following data segments to be located on paragraph (16 byte) boundary.

=over 4

=item B<-P> I<address>

I<address> is the address where code segment of the
following object files will be placed. If an object file specification
is not preceded by B<-P> option, code segment will be appended to
the previous one. If no B<-P> option specified before the first
object file, its code segment will be placed to 0000.

=item B<-D> I<address>

I<address> is the address where data segment of the
following object files will be placed. If an object file specification
is not preceded by B<-D> option, data segment will be appended to
the previous one. If no B<-D> option specified before the first
object file, its data segment will be appended the last code segment.

=item B<-C> I<name>,I<address>

I<address> is the address  where common block I<name>
will be placed. If block name I<name> is empty string or it begins
with space it will be treated as blank common block.
However no extra whitespace character around of comma (,) allowed.
I<name> is case insensitive.
Common blocks of unspecified location will be appended to the
last data segment.

=item B<-U> I<name>

Common block named I<name> will be "uncommon". (This sounds funny,
doesn't it? :-) Normally common blocks of the same name are to be
located on the same address. However blocks marked as uncommon
will be concatenated like code or data segments. This way you
can spread your code/data over several region of physical memory easy.
I<name> is case insensitive.

=item B<-l>

The following object file is a library. B<ld80> will scan the
file and loads modules only that satisfies unresolved external references.
If a new reference to a modules passed earlier found the referenced
module will not be loaded. However you can specify the same library
more than one time in order to resolve backward references.

=item B<-c>

Suppress data segments. The output file will contain the
the code (and absolute) segments only.

=item B<-O> I<oformat>

Output format specification. Possible values of I<oformat> are:

B<ihex>: Intel hex. This is the default format.

B<bin>: Raw binary, gaps filled with X'00'.

B<binff>: Raw binary, gaps filled with X'ff'.

=item B<-W> I<warns>

Request for warning messages. Possible value of I<warns> is:

B<extchain>: Warn if ambigous external reference found. See L<"WARNINGS">.

=item B<-s> I<symfile>

Name of symbol file. `-' stands for the standard output.

=item B<-m>

Generate map. List of segment mapping will be placed into symbol file
if it is specified. Otherwise the segment list will printed on standard
output.

=item B<-S> I<symsize>

Symbol table size. Use this option if the linker have to process more
than 1024 symbols.

=item B<-V>

Print version number and exit.

=back

=head1 WARNINGS

The object file format used by L80 is ambigous. References
to the same external symbol are chained. The last element of the chain
is absolute 0000. M80 produces the same object file from the
following assembly sources:

	;program A			;program B
	EXTRN	foobar			EXTRN	foobar
	ASEG				ASEG
	ORG	0			ORG	0
	DW	foobar			DW	0
	END				END

L80 and ld80 cannot figure out what was your intention therefore
always assumes the B version. However ld80 can warn you if this
ambiguity found. Use B<-W extchain> option to achieve this.

Placing external reference to location absolute 0000 is possible but
a bit tricky:

	;program C
	EXTRN	foobar
	ASEG
	ORG	0
	DW	foobar/1	; forced arithmetic fixup
	END

In this case M80 places a newer (and more complicated) item
into the object that can be handle unambigously.

=head1 RESTRICTIONS

B<ld80> does not process special link item 13 (request library search).
If the linker finds such an item, it prints a warning message and continues
the work.

Special link item 12 (chain address) is also unimplemented.
This is because the author has never seen this kind of item. :-)
B<ld80> will abort if it finds one.

Extension link item X'35' (COBOL overlay segment sentinel) is also
unimplemented due to the same reason. However other extension
link items (known as arithmetic fixups) are fully implemented.

=head1 PORTING

B<ld80> is written for and developed on Intel-Linux platform.
It requires some functions of glibc2. It probably could run on big endian
machines without any change, but this was never tested.

=head1 BUGS

This manual is written in Tarzan-English.

=head1 SEE ALSO

=for html
M$ Utility Software Manual. It can be found on the net as file 
<a href="ftp://ftp.funet.fi/pub/msx/programming/asm/m80l80.txt">m80l80.txt</a>.

=for roff
M$ Utility Software Manual. It can be found on the net as file
\fIm80l80.txt\fR.

=head1 AUTHOR

GE<aacute>bor Kiss E<lt>kissg@sztaki.huE<gt>

=head1 COPYRIGHT

This software is copylefted.

=head1 DISCLAIMER

 This software is in the public domain.
 This software is distributed with no warranty whatever.
 The author takes no responsibility for the consequences of its use.

