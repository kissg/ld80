#ifndef	_AR80_H_
#define	_AR80_H_

#include "ld80.h"
#include "obj80.h"
#include <sys/stat.h>

enum cmd_code {CMD_NONE,
		CMD_DELETE, CMD_MOVE, CMD_APPEND, CMD_REPLACE,
		CMD_TABLE, CMD_EXTRACT};

struct arch_member {
	struct arch_member *next;
	struct object_item *head;
	struct stat st;
	bitpos_t bitpos;
	size_t bitsize;
	char *basename;
	char *path;
	unsigned flags;
#define	MEMBER_TO_DELETE	1
#define	MEMBER_AFTER		2
#define	MEMBER_REPLACES		4
#define	MEMBER_IN_ARGV		8
};

extern enum cmd_code arcmd;
extern int verbose;

BITFILE *open_tmp(void);
void copy_tmp(char *libfilename);
void write_module(struct object_item *, BITFILE *);
void write_eof(BITFILE *);
void list_archive(struct arch_member *member, char **argv);
struct object_item *copy_lib(BITFILE *libfile, BITFILE *outfile,
	struct object_item *head, char *before, char *after,
	char mode, char **exceptions);
int add_member(BITFILE *outfile, char *objfilename, char mode);
void save_module(struct object_item *head, int orig);
void save_all_modules(BITFILE *libfile, int orig);
void extract_members(BITFILE *libfile, char **argv, struct object_item **modv);
struct arch_member *read_member(BITFILE *bf, char *filename);
int write_member(struct arch_member *member, BITFILE *outfile);

#endif
