#ifndef	_LD80_H_
#define	_LD80_H_

#include "obj80.h"
#ifdef DEBUG
#define	IFDEBUG(level, statement) if (debug >= level) statement
#else
#define IFDEBUG(level, statement)
#endif

#define	MAX_SEGMENTS	128
#define	MAX_COMMONS	(MAX_SEGMENTS-3)

/* exit codes */
#define E_SUCCESS	0	/* no error */
#define E_USAGE		1	/* command line error, incorrect usage */
#define E_INPUT		2	/* unprocessable input file */
#define E_RESOURCE	3	/* out of resources */
#define	E_SYSTEM	4	/* system error */

/* output file formats */
#define	F_IHEX		0	/* Intel HEX */
#define	F_BIN00		1	/* binary, gaps filled with 00 */
#define	F_BINFF		2	/* binary, gaps filled with FF */

struct loc {
	struct section *section;
	int offset;
};

struct fixup {
	struct fixup *next;
	int lc;
	struct loc at;
};

struct section {
	struct section *next;
	int base;	/* -1 = undefinded */
	int lc;		/* current lc */
	unsigned char *buffer;
	int len;	/* final lc - start */
	char *filename;
	struct fixup *fixups;
	struct node *nodehead, *nodetail;
	char module_name[NAMELEN+1];
	struct segment *segment;
};

struct segment {
	int type;	/* absolute, code, data, common */
	struct section *secs;
	int default_base;
	int uncommon;
	int maxsize;
	char common_name[NAMELEN+1];
};

struct symbol {
	struct loc at;
	int value;
#define	UNDEFINED	(-1)
	char name[NAMELEN+1];
};

struct node {
	struct node *next;
	struct loc at;
	int nodenum;
	int type;
#	define	N_OPERAND	0
#	define	N_BYTE		1
#	define	N_WORD		2
#	define	N_HIGH		3
#	define	N_LOW		4
#	define	N_NOT		5
#	define	N_UNARYMINUS	6
#	define	N_MINUS		7
#	define	N_PLUS		8
#	define	N_MULT		9
#	define	N_DIV		10
#	define	N_MOD		11
#	define	N_EXTERNAL	N_OPERAND+128
#	define	N_EXTMINUS	N_MINUS+128
#	define	N_EXTPLUS	N_PLUS+128
	int value;
	struct symbol *symbol;
};

extern char *progname;
extern int warn_extchain, debug;
extern int fatalerror;
extern struct segment *segv;
extern unsigned char usage_map[];
#define MARK_BYTE(a)	usage_map[(a)/8] |= 1 << ((a)%8)
#define MARKED(a)	(usage_map[(a)/8] & 1 << ((a)%8))
#define MARKED32(a)	(((long*)usage_map)[(a)/32] == 0xffffffff)
#define UNMARKED32(a)	(((long*)usage_map)[(a)/32] == 0)
extern long objectpos;

void die(int, const char*, ...) __attribute__ ((__noreturn__));
void *calloc_or_die(size_t, size_t);

int read_object_file(char *, int);

void set_base_address(int, char *, int, int);
void mark_uncommon(char *);
void add_item(struct object_item *, char *, BITFILE *);
void relocate_sections(void);
void dump_sections(void);
void init_section(void);
void join_sections(int);
void print_map(FILE *);

struct symbol *find_symbol(char *);
int add_symbol(char *, int, struct section *);
struct symbol *get_symbol(char *);
int init_symbol(int);
void clear_symbol(void);
void dump_symbols(void);
void set_symbols(void);
void print_symbol_table(FILE *);

void add_fixup(struct section *, struct section *, int);
void set_fixups(void);
void resolve_externals(void);
void convert_chain_to_nodes(char *, int, struct section *);
void process_nodes(void);
struct node *add_node(struct section *, int, int);

int do_out(FILE *, int);

#endif
