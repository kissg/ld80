#include <stdio.h>
#include <errno.h>
//#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <malloc.h>
#include "obj80.h"
#include "ld80.h"
#include "bitstream.h"

#define	HAS_A		1
#define	HAS_B		2
#define	ALIGN		4
#define	BLANK		8
static int special_attrib[16] = {
	HAS_B, HAS_B|BLANK, HAS_B, HAS_B, HAS_B,	/* 0-4 */
	HAS_A|HAS_B|BLANK, HAS_A|HAS_B, HAS_A|HAS_B,	/* 5-7 */
	HAS_A, HAS_A, HAS_A, HAS_A, HAS_A, HAS_A,	/* 8-13 */
	HAS_A|ALIGN, 					/* 14 */
	0,						/* 15 */
};

unsigned long read_item(struct object_item *item, BITFILE *bf)
{
	unsigned char i;
	static struct object_item end_of_file = {
		type: T_RELOCATABLE|T_SPECIAL,
		v: { special: { control: C_END_FILE } }
	};


	item->pos = bit_tell(bf);
	if (bit_eof(bf, BIT_EOF_GET)) {
		memcpy(item, &end_of_file, sizeof(end_of_file));
		return S(item->v.special.control);
	}


	bit_read(&i, 1, bf);
	if (i==0) {	/* absolute */
		item->type = T_ABSOLUTE;
		bit_read(&item->v.absolute_byte, 8, bf);
		return ABS;
	}

	/* relocatable */
	bit_read(&i, 2, bf);
	item->type = T_RELOCATABLE | i;
	if (i != 0) {	/* relative */
		bit_read((unsigned char *)&item->v.relative_word, 16, bf);
		return RELOC;
	}

	/* special link item */
	bit_read(&i, 4, bf);
	item->v.special.control = i;
	if (special_attrib[item->v.special.control] & HAS_A) {
		bit_read((unsigned char *)&item->v.special.A_t, 2, bf);
		bit_read((unsigned char *)&item->v.special.A_value, 16, bf);
	}
	if (special_attrib[item->v.special.control] & HAS_B) {
		int j;
		unsigned char len;

		bit_read(&len, 3, bf);
		if (!len) len = 8;
		item->v.special.B_len = len;
		for (j=0; j<len; j++) {
			bit_read(&item->v.special.B_name[j], 8, bf);
		}
		if (item->v.special.B_name[0]==' ' &&
			special_attrib[item->v.special.control] & BLANK) j=0;
		item->v.special.B_name[j] = '\0';
	}
	else {
		item->v.special.B_name[0] = '\0';
		item->v.special.B_len = 0;
	}
	if (special_attrib[item->v.special.control] & ALIGN)
		bit_align(bf);
	return S(item->v.special.control);
}

struct object_item *read_module(BITFILE *bf, size_t *lenptr)
{
	struct object_item *head, *new;
	unsigned long entry_type;
	bitpos_t begin;

	head = new = malloc(sizeof(struct object_item));
	if (new==NULL) die(E_RESOURCE, "%s: Not enough memory\n", progname);
	new->next = NULL;

	begin = bit_tell(bf);
	entry_type = read_item(new, bf); 
	IFDEBUG(3, dump_item("<<", new));
	if (entry_type == EOF_ELEMENT) {
		bit_eof(bf, BIT_EOF_SET);
		free(new);
		return NULL;
	}

	while (entry_type != EOP_ELEMENT) {
		struct object_item *tail;

		tail = new;
		new = malloc(sizeof(struct object_item));
		if (new==NULL)
			die(E_RESOURCE, "%s: Not enough memory\n", progname);
		new->next = NULL;
		entry_type = read_item(new, bf); 
		IFDEBUG(3, dump_item("<<", new));

		tail->next = new;
	}
	if (lenptr) *lenptr = bit_tell(bf) - begin;
	return head;
}

void free_module(struct object_item *head)
{
	struct object_item *next;
	while(head) {
		next = head->next;
		free(head);
		head = next;
	}
}

#ifdef	DEBUG

static char *stypes[] = {
	"special","code","data","common",
};
static char *atypes[] = {
	"absolute","code","data","common",
};
static char *operators[] = {
	"",
	"BYTE",
	"WORD",
	"HIGH",
	"LOW",
	"NOT",
	"unary -",
	"-",
	"+",
	"*",
	"/",
	"MOD",
};

void dump_item(char *dir, struct object_item *item)
{
	printf("%s ",dir);
	switch (item->type) {
	case T_ABSOLUTE:
		printf("absolute %.2x\n",item->v.absolute_byte);
		break;
	case T_RELOCATABLE|T_CODE:
	case T_RELOCATABLE|T_DATA:
	case T_RELOCATABLE|T_COMMON:
		printf("%s relative %.4x\n",
			stypes[item->type&T_MASK],
			item->v.relative_word);
		break;
	case T_RELOCATABLE|T_SPECIAL:
		switch (item->v.special.control) {
		case C_ENTRY_SYMBOL:
			printf("Entry symbol %s\n",item->v.special.B_name);
			break;
		case C_SELECT_COMMON:
			printf("Select common %s\n",item->v.special.B_name);
			break;
		case C_PROGNAME:
			printf("Program name %s\n",item->v.special.B_name);
			break;
		case C_LIBSEARCH:
			printf("Library search %s\n",item->v.special.B_name);
			break;
		case C_EXTENSION:
			printf("Extension: ");
			switch (item->v.special.B_name[0]) {
			int operator;
			case 'A':
				operator = item->v.special.B_name[1];
				if (operator > 11) die(E_INPUT,
					"%s: Unknow operator %.2x\n",
					progname, operator);
				printf(" operator \"%s\"\n",
					operators[operator]);
				break;
			case 'B':
				printf(" operand %s (external)\n",
					item->v.special.B_name+1);
				break;
			case 'C':
				printf(" operand %.4x (%s)\n",
					(item->v.special.B_name[3] << 8) +
					item->v.special.B_name[2],
					atypes[item->v.special.B_name[1]]);
				break;
			case 'p':
				{ char buf[] = "rwxrwxrwx";
				int i;
				int p = mode_item(item);
				for (i=0; i<9; i++)
					if (!(p&(1<<i))) buf[8-i] = '-';
				printf(" permissions %s\n", buf);
				}
				break;
			case 't':
				{ char buf[18];
				time_t t = 0;

				t = time_item(item);
				strftime(buf, sizeof(buf), "%h %e %R %Y",
						localtime(&t));
				printf(" date %s\n", buf);
				}
				break;
			case 'o':
				printf(" uid %d\n", owner_item(item));
				break;
			case 'g':
				printf(" gid %d\n", group_item(item));
				break;
			default:
				die(E_INPUT, "%s: Unknown extension %.2x\n",
					progname, item->v.special.B_name[0]);
			}
			break;
		case C_COMMON_SIZE:
			printf("Common %s size %.4x (%s)\n",
				item->v.special.B_name,
				item->v.special.A_value,
				atypes[item->v.special.A_t]);
			break;
		case C_CHAIN_EXTERNAL:
			printf("Chain external symbol %s to %.4x (%s)\n",
				item->v.special.B_name,
				item->v.special.A_value,
				atypes[item->v.special.A_t]);
			break;
		case C_ENTRY_POINT:
			printf("Entry point %s is %.4x (%s)\n",
				item->v.special.B_name,
				item->v.special.A_value,
				atypes[item->v.special.A_t]);
			break;
		case C_EXT_MINUS_OFF:
			printf("External minus offset %.4x (%s)\n",
				item->v.special.A_value,
				atypes[item->v.special.A_t]);
			break;
		case C_EXT_PLUS_OFF:
			printf("External plus offset %.4x (%s)\n",
				item->v.special.A_value,
				atypes[item->v.special.A_t]);
			break;
		case C_DATA_SIZE:
			printf("Data size is %.4x (%s)\n",
				item->v.special.A_value,
				atypes[item->v.special.A_t]);
			break;
		case C_SET_LC:
			printf("Set location counter to %.4x (%s)\n",
				item->v.special.A_value,
				atypes[item->v.special.A_t]);
			break;
		case C_CHAIN_ADDRESS:
			printf("Chain address %.4x (%s)\n",
				item->v.special.A_value,
				atypes[item->v.special.A_t]);
			break;
		case C_PROG_SIZE:
			printf("Code size is %.4x (%s)\n",
				item->v.special.A_value,
				atypes[item->v.special.A_t]);
			break;
		case C_END_PROGRAM:
			printf("Program end is %.4x (%s)\n\n",
				item->v.special.A_value,
				atypes[item->v.special.A_t]);
			break;
		case C_END_FILE:
			printf("End of file\n");
			break;
		default:
			die(E_INPUT, "%s: Unknown control type %x\n",
				progname, item->v.special.control);
		}
		break;
	default:
		die(E_INPUT, "%s: Illegal item type %x\n",
			progname, item->type);
	}
}
#endif

void write_item(struct object_item *item, BITFILE *bf)
{
	int i;

	IFDEBUG(3, dump_item(">>", item));

	switch (item->type) {
	case T_ABSOLUTE:
		bit_write(0, 1, bf);
		bit_write(item->v.absolute_byte, 8, bf);
		break;
	case T_RELOCATABLE|T_CODE:
	case T_RELOCATABLE|T_DATA:
	case T_RELOCATABLE|T_COMMON:
		bit_write(1, 1, bf);
		bit_write(item->type&T_MASK, 2, bf);
		bit_write( item->v.relative_word       & 0xff, 8, bf);
		bit_write((item->v.relative_word >> 8) & 0xff, 8, bf);
		break;
	case T_RELOCATABLE|T_SPECIAL:
		bit_write(0x04, 3, bf);
		bit_write(item->v.special.control, 4, bf);
		if (special_attrib[item->v.special.control] & HAS_A) {
			bit_write(item->v.special.A_t, 2, bf);
			bit_write( item->v.special.A_value       & 0xff, 8, bf);
			bit_write((item->v.special.A_value >> 8) & 0xff, 8, bf);
		}
		if (special_attrib[item->v.special.control] & HAS_B) {
			bit_write(item->v.special.B_len, 3, bf);
			for (i=0; i<item->v.special.B_len; i++)
				bit_write(item->v.special.B_name[i], 8, bf);
		}
		if (special_attrib[item->v.special.control] & ALIGN)
			bit_align(bf);
		break;
	}
}

int mode_item(struct object_item *item)
{
	return	item->v.special.B_name[1] | item->v.special.B_name[2] << 8;
}

time_t time_item(struct object_item *item)
{
	time_t t = 0;
	int i;

	if (sizeof(time_t) > 4) die(E_SYSTEM,
		"%s: time_t longer than 4 bytes\n", progname);
	for (i=0; i<sizeof(time_t); i++)
		t |= item->v.special.B_name[i+1] << i*8;
	return t;
}

uid_t owner_item(struct object_item *item)
{
	uid_t u = 0;
	int i;

	if (sizeof(uid_t) > 4) die(E_SYSTEM,
		"%s: uid_t longer than 4 bytes\n", progname);
	for (i=0; i<sizeof(uid_t); i++)
		u |= item->v.special.B_name[i+1] << i*8;
	return u;
}

gid_t group_item(struct object_item *item)
{
	gid_t g = 0;
	int i;

	if (sizeof(gid_t) > 4) die(E_SYSTEM,
		"%s: gid_t longer than 4 bytes\n", progname);
	for (i=0; i<sizeof(gid_t); i++)
		g |= item->v.special.B_name[i+1] << i*8;
	return g;
}
