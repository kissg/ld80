#ifndef	_BITSTREAM_H_
#define	_BITSTREAM_H_

typedef	unsigned long bitpos_t;

struct _BIT_FILE {
	FILE *f;
	bitpos_t bitpos;
	unsigned char bytebuf;
	int flags;
#define	BIT_WRITE	1
#define	BIT_EOF		2
};

#define	BIT_EOF_GET	0
#define	BIT_EOF_SET	1

typedef struct _BIT_FILE BITFILE;

#define	align_len(bits)	(-((-(bits))&~(8-1)))

BITFILE *bit_open(const char *path, const char *mode);
int put_bit(unsigned bit, BITFILE *bf);
int get_bit(BITFILE *bf);
int bit_write(unsigned long bits, int len, BITFILE *bf);
int bit_read(unsigned char *bits, int len, BITFILE *bf);
int bit_align(BITFILE *bf);
bitpos_t bit_tell(BITFILE *bf);
int bit_seek(BITFILE *bf, bitpos_t offset, int whence);
int bit_close(BITFILE *bf);
int bit_eof(BITFILE *bf, int cmd);

#endif
