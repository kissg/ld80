#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include "ld80.h"
#include "bitstream.h"
#include "obj80.h"

static char *objfilename;

int read_object_file(char *filename, int lib)
{
	int modcnt = 0;
	BITFILE *objectfile;

	objectfile=bit_open(filename,"r");
	if (objectfile==NULL)
		die(E_USAGE, "%s: Cannot open object file %s: %s\n",
			progname, filename, sys_errlist[errno]);

	objfilename = filename;
	while(read_module(objectfile, lib, filename, add_item)) modcnt++;
	bit_close(objectfile);
	return modcnt;
}

