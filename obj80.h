#ifndef	_OBJ80_H_
#define	_OBJ80_H_

#include <sys/types.h>
#include "bitstream.h"

#define S(special_type) (1<<(special_type))
#define RELOC           (1<<16)
#define ABS             (1<<17)

#define PRGNAME_ELEMENT S(2)
#define HEAD_ELEMENT    (S(0)|S(5)|S(10)|S(13))
#define BODY_ELEMENT    (S(1)|S(3)|S(4)|S(8)|S(9)|S(11)|RELOC|ABS)
#define TAIL_ELEMENT    (S(1)|S(6)|S(7)|S(12)|S(14))
#define EOP_ELEMENT     (S(14))
#define EOF_ELEMENT     (S(15))

#define	NAMELEN		8

/* segment types */
#define	T_ABSOLUTE	0x00
#define	T_RELOCATABLE	0x10
#define	T_MASK		0x03
#define	T_SPECIAL	0
#define	T_CODE		1
#define	T_DATA		2
#define	T_COMMON	3

struct object_item {
	struct object_item *next;	// must be first element
	bitpos_t pos;
	unsigned char type;	/* absolute, special, code, data, common */
	union {
		unsigned char absolute_byte;
		unsigned short relative_word;
		struct {
			int control;
#			define C_ENTRY_SYMBOL	0
#			define C_SELECT_COMMON	1
#			define C_PROGNAME	2
#			define C_LIBSEARCH	3
#			define C_EXTENSION	4
#			define C_COMMON_SIZE	5
#			define C_CHAIN_EXTERNAL	6
#			define C_ENTRY_POINT	7
#			define C_EXT_MINUS_OFF	8
#			define C_EXT_PLUS_OFF	9
#			define C_DATA_SIZE	10
#			define C_SET_LC		11
#			define C_CHAIN_ADDRESS	12
#			define C_PROG_SIZE	13
#			define C_END_PROGRAM	14
#			define C_END_FILE	15
			unsigned short A_value;
			unsigned char A_t;
			unsigned char B_len;
			unsigned char B_name[NAMELEN+1];
		} special;
	} v;
};

void dump_item(char *, struct object_item *);
unsigned long read_item(struct object_item *item, BITFILE *bf);
struct object_item *read_module(BITFILE *bf, size_t *lenptr);
void free_module(struct object_item *head);
void write_item(struct object_item *item, BITFILE *bf);

int mode_item(struct object_item *item);
time_t time_item(struct object_item *item);
uid_t owner_item(struct object_item *item);
gid_t group_item(struct object_item *item);

#endif // _OBJ80_H_
